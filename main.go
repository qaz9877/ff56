package main

import (
	"ff56/cof"
	"ff56/thr"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
)

// a.Settings().SetTheme(&cof.MyTheme{})
func main() {
	a := app.New()
	a.Settings().SetTheme(&cof.MyTheme{})
	w := a.NewWindow("你好")

	// hello := widget.NewLabel("你好")
	// w.SetContent(container.NewVBox(
	// 	hello,
	// 	widget.NewButton("Hi!", func() {
	// 		hello.SetText("Welcome :)")
	// 	}),
	// ))
	tabs := container.NewAppTabs(
		container.NewTabItem("视频", thr.VidScreenn(w)),
		container.NewTabItem("音乐", thr.MusScreenn(w)),
		container.NewTabItem("图片", thr.PicScreenn(w)),
	)
	w.SetContent(tabs)
	w.Resize(fyne.NewSize(800, 700))
	w.ShowAndRun()
}

// func makeNavVideo(w fyne.Window) fyne.CanvasObject {
// 	tutorial := container.NewBorder(nil, nil, nil, nil, container.NewStack(thr.VidScreenn(w)))
// 	split := container.NewHSplit(container.NewStack(widget.NewLabel("你好")), tutorial)
// 	split.Offset = 0.2
// 	return split
// }

// func makeNavMisic(w fyne.Window) fyne.CanvasObject {
// 	tutorial := container.NewBorder(nil, nil, nil, nil, container.NewStack(thr.MusScreenn(w)))
// 	split := container.NewHSplit(container.NewStack(widget.NewLabel("你好")), tutorial)
// 	split.Offset = 0.2
// 	return split
// }

// func makeNavPicture(w fyne.Window) fyne.CanvasObject {
// 	tutorial := container.NewBorder(nil, nil, nil, nil, container.NewStack(thr.PicScreenn(w)))
// 	split := container.NewHSplit(container.NewStack(widget.NewLabel("你好")), tutorial)
// 	split.Offset = 0.2
// 	return split
// }
