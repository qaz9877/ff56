package thr

import (
	"net/url"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func parseURL(urlStr string) *url.URL {
	link, err := url.Parse(urlStr)
	if err != nil {
		fyne.LogError("Could not parse URL", err)
	}

	return link
}
func VidScreenn(_ fyne.Window) fyne.CanvasObject {

	hello := container.NewStack(widget.NewLabel("你好"))
	content := container.NewVScroll(
		container.NewVBox(
			widget.NewHyperlink("fyne.io", parseURL("https://fyne.io/")),
			widget.NewLabel("ssss"),
			widget.NewLabel(""),
		))
	tutorial := container.NewBorder(nil, nil, nil, nil, container.NewStack(content))
	split := container.NewHSplit(hello, tutorial)
	split.Offset = 0.2
	return split
}
