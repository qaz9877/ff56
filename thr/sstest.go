package thr

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

// []fyne.CanvasObject
func TestScreenn(_ fyne.Window) fyne.CanvasObject {
	vlist := []fyne.CanvasObject{
		widget.NewButton("test", func() {

		}),
		widget.NewButton("test", func() {

		}),
	}
	vert := container.NewBorder(nil, nil, nil, nil, container.NewVScroll(container.NewVBox(vlist...)))
	//hello := container.NewStack(widget.NewLabel("你好"))
	content := container.NewVScroll(
		container.NewVBox(
			widget.NewHyperlink("fyne.io", parseURL("https://fyne.io/")),
			widget.NewLabel("ssss"),
			widget.NewLabel(""),
		))
	tutorial := container.NewBorder(nil, nil, nil, nil, container.NewStack(content))
	split := container.NewHSplit(vert, tutorial)
	split.Offset = 0.2
	return split
}
