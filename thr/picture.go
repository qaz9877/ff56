package thr

import (
	"fmt"
	"image/color"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

var children []string = []string{}
var MuL string = ""
var indexchild int = 0
var content = container.NewStack()
var content2 = container.NewStack()

// []fyne.CanvasObject
func PicScreenn(w fyne.Window) fyne.CanvasObject {
	red := color.NRGBA{R: 0xff, A: 0xff}
	text1 := canvas.NewText("1", red)
	text2 := canvas.NewText("2", red)
	text3 := canvas.NewText("3", red)
	text4 := canvas.NewText("4", red)
	grid := container.New(layout.NewVBoxLayout(), container.NewHBox(text1, layout.NewSpacer(), text2, layout.NewSpacer()), container.NewHBox(text3, layout.NewSpacer(), text4, layout.NewSpacer()))

	content.Objects = []fyne.CanvasObject{container.NewVScroll(grid)}
	content.Refresh()
	vlist := []fyne.CanvasObject{
		widget.NewButton("导入图片", func() {
			dialog.ShowFolderOpen(func(list fyne.ListableURI, err error) {
				if err != nil {
					dialog.ShowError(err, w)
					return
				}
				if list == nil {
					log.Println("Cancelled")
					return
				}

				fynechildren, err := list.List()
				if err != nil {
					dialog.ShowError(err, w)
					return
				}
				MuL = strings.TrimPrefix(list.String(), "file://")
				for _, child := range fynechildren {
					children = append(children, removeFilePrefix(list.String()+"/"+child.Name()))
				}
				// out := fmt.Sprintf("Folder %s (%d children):\n%s", list.Name(), len(children), list.String())
				// dialog.ShowInformation("Folder Open", out, w)
				grid := makeva(w, children)
				content.Objects = []fyne.CanvasObject{container.NewVScroll(grid)}
				content.Refresh()
			}, w)
		}),
		widget.NewButton("清空清空", func() {
			children = []string{}
			MuL = ""
			content.Objects = []fyne.CanvasObject{container.NewVScroll(grid)}
			content.Refresh()
		}),
		widget.NewButton("批量保存", func() {
			dialog.ShowFolderOpen(func(list fyne.ListableURI, err error) {
				if err != nil {
					dialog.ShowError(err, w)
					return
				}
				if list == nil {
					log.Println("Cancelled")
					return
				}

				// fynechildren, err := list.List()
				// if err != nil {
				// 	dialog.ShowError(err, w)
				// 	return
				// }
				MuLnew := strings.TrimPrefix(list.String(), "file://")
				err = copyFiles(MuL, MuLnew)
				if err != nil {
					dialog.ShowError(err, w)
					return
				}
			}, w)

		}),
		widget.NewButton("下一张", func() {
			fmt.Println(len(children))
			if len(children) != 0 && indexchild < len(children) {
				showImage(indexchild, children[indexchild])
				indexchild++
			} else {
				dialog.ShowInformation("到底了", "到底了", w)
				indexchild--
			}
		}),
		widget.NewButton("上一张", func() {
			if len(children) != 0 && indexchild < len(children) && indexchild >= 0 {
				showImage(indexchild, children[indexchild])

				if indexchild != 0 {
					indexchild--
				}

			} else {
				if indexchild >= len(children) {
					indexchild--
				}
				dialog.ShowInformation("到底了", "到底了", w)

			}
		}),
		content2,
	}
	vert := container.NewBorder(nil, nil, nil, nil, container.NewVScroll(container.NewVBox(vlist...)))
	//hello := container.NewStack(widget.NewLabel("你好"))
	// title := widget.NewLabel("Component name")
	// title2 := widget.NewLabel("Component name")
	tutorial := container.NewBorder(nil, nil, nil, nil, content)
	split := container.NewHSplit(vert, tutorial)
	split.Offset = 0.2
	return split
}

func makeva(w fyne.Window, childrens []string) fyne.CanvasObject {
	// grid := new(fyne.CanvasObject)
	containers := make([]fyne.CanvasObject, 0)
	for index, child := range childrens {

		red := color.NRGBA{R: 0xff, A: 0xff}
		text := canvas.NewText(child, red)
		text2 := canvas.NewText(strconv.Itoa(index+1), red)
		// fmt.Println(urlStr + "//" + child.Name())
		//dst := "C:/1711980228317 (2) - 副本.png"
		src := child

		indexnew := index
		// err := copyFile(src, dst)
		// if err != nil {
		// 	fmt.Println("复制文件时出错:", err)
		// }
		button1 := widget.NewButton("预览", func() {
			indexchild = indexnew
			showImage(indexnew, src)
		})
		button2 := widget.NewButton("删除", func() {
			err := deleteFile(src)
			if err != nil {
				dialog.ShowError(err, w)
				return
			}
			if MuL != "" {
				children, err = listFilesInDir(MuL)
				fmt.Println("数组长度", len(children))
				if err != nil {
					dialog.ShowError(err, w)
					return
				}
			}
			grid := makeva(w, children)
			content.Objects = []fyne.CanvasObject{container.NewVScroll(grid)}
			content.Refresh()

		})
		button3 := widget.NewButton("另存", func() {

			saveDialog := dialog.NewFileSave(func(uc fyne.URIWriteCloser, err error) {
				if err != nil {
					log.Println("Error saving file:", err)
					return
				}
				if uc == nil {
					log.Println("Cancelled")
					return
				}
				data, err := ioutil.ReadFile(src)
				if err != nil {
					fmt.Println(src)
					fyne.LogError("Failed to load image data", err)
					return
				}
				// 获取文件内容
				// content := "Hello, World!\n" // 这里替换为你想要保存的内容

				// 将文件内容写入到用户选择的文件中
				_, err = uc.Write(data)
				if err != nil {
					log.Println("Error writing to file:", err)
					return
				}

				// 关闭文件
				err = uc.Close()
				if err != nil {
					log.Println("Error closing file:", err)
					return
				}

				log.Println("File saved successfully!")

			}, w)
			saveDialog.SetFileName(filepath.Base(src)) // 设置默认文件名

			saveDialog.Show()
			var err error
			if MuL != "" {
				children, err = listFilesInDir(MuL)
				if err != nil {
					dialog.ShowError(err, w)
					return
				}
			}
			grid := makeva(w, children)
			content.Objects = []fyne.CanvasObject{container.NewVScroll(grid)}
			content.Refresh()
		})
		button4 := widget.NewButton("改名", func() {

			username := widget.NewEntry()
			username1 := widget.NewEntry()
			username1.SetText(strings.TrimSuffix(filepath.Base(src), filepath.Ext(filepath.Base(src))))

			items := []*widget.FormItem{
				widget.NewFormItem("老", username1),
				widget.NewFormItem("新", username),
			}
			dialog.ShowForm("Login...", "确定", "Cancel", items, func(b bool) {
				if !b {
					return
				}

				log.Println("Please Authenticate", username.Text)
				err := renameFile(src, username.Text+"."+filepath.Ext(src))
				if err != nil {
					dialog.ShowError(err, w)
					return
				}
				if MuL != "" {
					children, err = listFilesInDir(MuL)
					fmt.Println("数组长度", len(children))
					if err != nil {
						dialog.ShowError(err, w)
						return
					}
				}
				grid := makeva(w, children)
				content.Objects = []fyne.CanvasObject{container.NewVScroll(grid)}
				content.Refresh()
			}, w)

		})
		//  container.NewHBox(, layout.NewSpacer(), , layout.NewSpacer())
		container := container.NewHSplit(container.NewHBox(text2, widget.NewLabel("-"), text), container.NewHBox(button1, button2, button3, button4))
		container.Offset = 0.7
		containers = append(containers, container)
		//fmt.Println(child.Name())
		if index == len(children) {
			indexchild = 0
		}
	}

	return container.NewVBox(containers...)
}

// renameFile 将指定文件重命名为新的文件名
func renameFile(oldFilePath, newFileName string) error {
	// 获取旧文件路径的目录部分
	dir := filepath.Dir(oldFilePath)

	// 将新文件名拼接到目录部分上，得到新文件路径
	newFilePath := filepath.Join(dir, newFileName)

	// 修改文件名称
	err := os.Rename(oldFilePath, newFilePath)
	if err != nil {
		return err
	}

	return nil
}
func copyFiles(sourceDir, destinationDir string) error {
	// 获取源文件夹中的所有文件列表
	files, err := ioutil.ReadDir(sourceDir)
	if err != nil {
		return fmt.Errorf("failed to read source directory: %v", err)
	}

	// 遍历源文件夹中的所有文件
	for _, file := range files {
		sourceFilePath := filepath.Join(sourceDir, file.Name())
		destinationFilePath := filepath.Join(destinationDir, file.Name())

		// 读取源文件内容
		data, err := ioutil.ReadFile(sourceFilePath)
		if err != nil {
			log.Printf("Failed to read file %s: %v", sourceFilePath, err)
			continue
		}

		// 写入目标文件夹中的文件
		err = ioutil.WriteFile(destinationFilePath, data, 0644)
		if err != nil {
			log.Printf("Failed to write file %s: %v", destinationFilePath, err)
			continue
		}

		fmt.Printf("File %s copied to %s successfully\n", sourceFilePath, destinationFilePath)
	}

	return nil
}
func saveFile(window fyne.Window, filePath string) {
	// 显示文件保存对话框，让用户选择保存的文件路径
	fmt.Println(filePath)
	saveDialog := dialog.NewFileSave(func(uc fyne.URIWriteCloser, err error) {
		if err != nil {
			log.Println("Error saving file:", err)
			return
		}
		data, err := ioutil.ReadFile(filePath)
		if err != nil {
			fmt.Println(filePath)
			fyne.LogError("Failed to load image data", err)
			return
		}
		// 获取文件内容
		// content := "Hello, World!\n" // 这里替换为你想要保存的内容

		// 将文件内容写入到用户选择的文件中
		_, err = uc.Write(data)
		if err != nil {
			log.Println("Error writing to file:", err)
			return
		}

		// 关闭文件
		err = uc.Close()
		if err != nil {
			log.Println("Error closing file:", err)
			return
		}

		log.Println("File saved successfully!")

	}, window)
	saveDialog.SetFileName(filepath.Base(filePath)) // 设置默认文件名
	if fileSaveOSOverride(saveDialog) {
		return
	}
	saveDialog.Show()
}
func fileSaveOSOverride(*dialog.FileDialog) bool {
	return false
}

// 去除字符串中的"file://"前缀
func removeFilePrefix(path string) string {
	// 检查字符串是否以"file://"开头
	if strings.HasPrefix(path, "file://") {
		// 如果是，则去除前缀并返回
		return strings.TrimPrefix(path, "file://")
	}
	// 如果不是，则返回原始字符串
	return path
}

// deleteFile 删除指定路径的文件
func deleteFile(filePath string) error {
	// 删除文件
	err := os.Remove(filePath)
	if err != nil {
		return err
	}

	return nil
}
func listFilesInDir(dirPath string) ([]string, error) {
	// 读取目录中的所有文件信息
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}

	// 存储文件名的切片
	var fileNames []string

	// 遍历文件列表
	for _, file := range files {
		// 检查是否为文件
		if file.IsDir() {
			// 如果是目录，可以选择是否递归处理子目录
			// 这里省略了递归处理的部分
			continue
		}

		// 将文件名添加到切片中
		fileNames = append(fileNames, dirPath+"/"+file.Name())
	}

	return fileNames, nil
}

// 复制文件
func copyFile(src, dst string) error {
	sourceFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer sourceFile.Close()

	destinationFile, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer destinationFile.Close()

	_, err = io.Copy(destinationFile, sourceFile)
	return err
}

func loadImage(filePath string) *canvas.Image {
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Println(filePath)
		fyne.LogError("Failed to load image data", err)
		return nil
	}

	return canvas.NewImageFromResource(fyne.NewStaticResource(filePath, data))
}

var visibilityWindow fyne.Window = nil

func showImage(index int, f string) {

	img := loadImage(f)
	if img == nil {
		return
	}
	img.FillMode = canvas.ImageFillOriginal
	if visibilityWindow == nil {
		// fmt.Println("2222")
		visibilityWindow = fyne.CurrentApp().NewWindow(f)
		visibilityWindow.SetCloseIntercept(func() {
			visibilityWindow.Hide()
		})
		visibilityWindow.SetContent(container.NewScroll(container.NewCenter(container.NewVBox(widget.NewLabel(fmt.Sprintf("第%s张", strconv.Itoa(index+1))), img))))
		visibilityWindow.Resize(fyne.NewSize(400, 320))
		visibilityWindow.Show()
	} else {
		// fmt.Println("1111")
		visibilityWindow.Show()
		visibilityWindow.SetTitle(f)
		visibilityWindow.SetContent(container.NewScroll(container.NewCenter(container.NewVBox(widget.NewLabel(fmt.Sprintf("第%s张", strconv.Itoa(index+1))), img))))
		visibilityWindow.Content().Refresh()
	}
	content2.Objects = []fyne.CanvasObject{container.NewVBox(widget.NewLabel("当前目录："+MuL), widget.NewLabel("当前序号："+strconv.Itoa(indexchild+1)))}
	content2.Refresh()

}

func fileSaved(f fyne.URIWriteCloser, w fyne.Window, filePath string) {
	defer f.Close()
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Println(filePath)
		fyne.LogError("Failed to load image data", err)
		return
	}

	_, err = f.Write(data)
	if err != nil {
		dialog.ShowError(err, w)
	}
	err = f.Close()
	if err != nil {
		dialog.ShowError(err, w)
	}
	log.Println("Saved to...", f.URI())
}
